import Vue from 'vue'
import App from './App.vue'
//引入bootstrap的vue插件和样式
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
//引入第三方动画
import animated from 'animate.css'
//import 'bootstrap-vue/dist/bootstrap-vue.css'
//引入axios
import axios from 'axios'
//引入store
import store from './store'
//引入VueRouter
import VueRouter from 'vue-router'
//引入自定义的路由器
import router from './router/routerIndex'
//引入时间插件
import moment from 'moment'

Vue.config.productionTip = false
//应用bootstrap
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
//应用动画
Vue.use(animated)
//应用axios
Vue.prototype.axios = axios
// axios.defaults.withCredentials = true;
//应用VueRouter
Vue.use(VueRouter)
//应用时间插件
Vue.prototype.moment = moment



new Vue({
  render: h => h(App),
  store,
  router:router
}).$mount('#app')
