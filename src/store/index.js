//引入Vue核心库
import Vue from 'vue'
//引入Vuex
import Vuex from 'vuex'
//应用Vuex插件
Vue.use(Vuex)

//准备actions对象——响应组件中用户的动作
const actions = {}
//准备mutations对象——修改state中的数据
const mutations = {

	//从avater组件中传来的时钟信息
	setTime(state, time) {
		state.time = time;
	},

	//修改导航栏卡片信息的显示
	setNavShow(state, show) {
		state.locationCard.show = show;

	},

	//局部刷新操作
	reflash(state, show) {
		//console.log("我进来了");
		state.ifShow.show = show;
		if (state.editorCard.inDynamic) {
			state.editorCard.visible = show;
		}

	},

	//下滑时自动收起用户卡片 
	setCollapsed(state, visible) {
		state.avaterCard.visible = visible;
	},

	//更改签到卡片状态
	setCheckCard(state) {
		console.log("我进来了");
		state.avaterCard.ifCheck = true;
		state.avaterCard.checkInText = "已签到";
		state.avaterCard.checkShow = false;
		state.avaterCard.day++;
	},

	//设置编辑框状态 
	setEditorCardVisible(state, res) {
		if (res) {
			//传来的是true说明在非动态list位置，就应当要使编辑框不可见，刷新也不可见
			state.editorCard.visible = false;
			state.editorCard.inDynamic = false;
		} else {
			state.editorCard.visible = true;
			state.editorCard.inDynamic = true;
		}

	},
	//加载完毕
	setLoadingVisible(state, visible) {
		state.ready = visible;
	},
	//设置导航栏吸顶状态
	setHeaderTop(state, visible) {
		state.headerIsTop = visible;
	},
	//设置点击了哪个动态模块
	setDynamicId(state,id){
		state.id = id;
	}

}
//准备state对象——保存具体的数据
const state = {
	//时钟js
	time: {
		year: "",
		month: "",
		day: "",
		hour: "",
		minute: "",
		second: "",
		week: "",
	},
	//右上角显示的相关数据
	locationCard: {
		show: true,
	},
	// 局部刷新
	ifShow: {
		show: true,
	},
	//用户卡片状态
	avaterCard: {
		visible: true,
		//是否签到
		ifCheck: false,
		//签到按钮是否显示
		checkShow: true,
		//签到文本
		checkInText: "签到",
		//签到天数
		day: 114,
		//动态数
		dynamicNum: 20,
		//站龄
		userDay: 2,
		//超越了百分之几的用户
		overWhelm: "100%",
	},
	//编辑框卡片状态
	editorCard: {
		visible: true,
		//是否在动态区
		inDynamic: true,
	},
	//用于设置动态卡片
	dynamicCard: [],
	//请求什么类型的动态
	id: 1,
	//设置loading状态
	ready: true,
	//设置导航栏吸顶状态
	headerIsTop: false,

}

//创建并暴露store
export default new Vuex.Store({
	actions,
	mutations,
	state
})