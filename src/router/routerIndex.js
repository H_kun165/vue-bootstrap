//引入VueRouter
import VueRouter from 'vue-router'
//注册组件
import MainDynamicCard from '../components/main/mainPage/main/MainDynamicCard'
// import MyDynamicCard from '../components/main/dynamic/MyDynamicCard'
import MyCircle from '../components/main/mainPage/navList/dynamic/MyCircle'
// import MyFollow from '../components/main/dynamic/MyFollow'
import TheRankingListOfUser from '../components/main/mainPage/navList/dynamic/TheRankingListOfUser'
import DynamcManagement from '../components/main/mainPage/navList/social/DynamicManagement'
import FriendManagement from '../components/main/mainPage/navList/social/FriendManagement'
import PrivateMessage from '../components/main/mainPage/navList/social/PrivateMessage'
import TheMessage from '../components/main/mainPage/navList/social/TheMessage.vue'
import MainPage from '../components/main/MainPage'
import MySpeace from '../components/personSpeace/MySpeace'
import TheRegister from '../components/login&register/TheRegister'
import TheLogin from '../components/login&register/TheLogin'
import MyTime from '../components/personSpeace/MyTime'
import MyNote from '../components/personSpeace/MyNote'
import EditorMyself from '../components/personSpeace/EditorMyself'
import ChatMain from '../components/chat/ChatMain'

// import App from '../App'




//创建router实例对象，去管理一组一组的路由规则
const router = new VueRouter({
	routes: [
		{
			path: '/',
			redirect: "/mainPage"
		},
		{
			path: '/mainPage',
			name: "home",
			component: MainPage,
			redirect: "/mainPage/mainDynamic",
			children: [
				{
					path: 'mainDynamic',
					component: MainDynamicCard,
				},
				{
					path: 'myDynamic',
					component: MainDynamicCard,
					// component: MyDynamicCard
				},
				{
					path: 'myCircle',
					component: MyCircle
				},
				{
					path: 'myFollow',
					component: MainDynamicCard,
					// component: MyFollow
				},
				{
					path: 'theRankingListOfUser',
					component: TheRankingListOfUser
				},
				{
					path: 'dynamicManagement',
					component: DynamcManagement,
				},
				{
					path: 'friendManagement',
					component: FriendManagement,
				},
				{
					path: 'privateMessage',
					component: PrivateMessage,
				},
				{
					path: 'theMessage',
					component: TheMessage,
				},
			]

		},
		{
			path: '/personSpeace',
			name: "personSpeace",
			title: "个人中心",
			component: MySpeace,
			redirect: '/personSpeace/myTime',
			children: [
				{
					path: 'myTime',
					name: 'myTime',
					title: "我的时间线",
					component: MyTime,
				},
				{
					path: 'myNote',
					name: 'myNote',
					title: "我的笔记",
					component: MyNote,
				},
				{
					path: 'editor',
					name: 'editor',
					title: "修改资料",
					component: EditorMyself,
				},

			]
		},
		{
			path: '/login',
			name: "login",
			title: "登录",
			component: TheLogin,
		},
		{
			path: '/register',
			name: "register",
			title: "注册",
			component: TheRegister,
		},
		{
			path: '/chat',
			name: "chat",
			title: "聊天室",
			component: ChatMain,
		},



	]
})

//暴露router
export default router