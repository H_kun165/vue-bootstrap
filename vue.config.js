const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  // devServer的配置
  devServer: {
    host: '0.0.0.0',
    port: 8080,
    historyApiFallback: true,
    allowedHosts: "all",
    open: false,
    client: {
      webSocketURL: 'ws://0.0.0.0:8080/ws',
    },
    headers: {
      'Access-Control-Allow-Origin': '*',
    },
    proxy: {
      '/api': {
        target: 'https://v1.hitokoto.cn/',
        changeOrigin: true,     // target是域名的话，需要这个参数，
        secure: true,          // 设置支持https协议的代理
        ws: false,
        pathRewrite: {
          '^/api': ''      // 这里可以理解为用‘/api’来代替target里面的地址，例如我们调用http://jspang.com/DemoApi/oftenGoods.php，直接写成‘/api/DemoApi/oftenGoods.php’就可以了
        }
      }
    },
  }
})

